from typing import Generator
from contextlib import contextmanager

import random
import textwrap
import base64
import sqlite3
import threading

from flask import Flask, Response, request, make_response
from waitress import serve

app: Flask = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False


DB_LOCK: threading.Lock = threading.Lock()


@contextmanager
def get_cursor() -> Generator[sqlite3.Cursor, None, None]:
    DB_LOCK.acquire(blocking=True, timeout=5)

    conn = sqlite3.connect("data/text.db")
    cursor = conn.cursor()
    cursor.execute(
        """
            CREATE TABLE IF NOT EXISTS stored_text
                (text_key TEXT PRIMARY KEY, value TEXT UNIQUE)
        """
    )
    conn.commit()
    yield cursor
    conn.commit()
    cursor.close()
    conn.close()

    DB_LOCK.release()


def generate_key() -> str:
    return base64.urlsafe_b64encode(random.randbytes(24)).decode("ascii")


def plaintext(text: str, status_code: int = 200) -> Response:
    response: Response = make_response(text, status_code)

    response.mimetype = "text/plain"
    response.charset = "utf-8"

    return response


@app.route("/store", methods=["POST"])
def store_text() -> Response:
    text: str | None = request.form.get("text")

    if text is None:
        return plaintext(
            "Hint: specify text in the `text` field of a URL-encoded POST body",
            400,
        )

    text_key: str

    random.seed(text)

    with get_cursor() as cursor:
        while True:
            text_key = generate_key()

            cursor.execute(
                "SELECT value FROM stored_text WHERE text_key=?",
                (text_key,),
            )

            found_text_result: tuple[str] | None = cursor.fetchone()

            if found_text_result is None:
                break

            if found_text_result[0] == text:
                return plaintext(text_key)

        # Store key and text
        cursor.execute(
            "INSERT INTO stored_text (text_key, value) VALUES (?, ?)",
            (text_key, text),
        )

    return plaintext(text_key)


@app.route("/<key>", methods=["GET"])
def get_string(key: str) -> Response:
    with get_cursor() as cursor:
        cursor.execute(
            "SELECT value FROM stored_text WHERE text_key=?", (key,)
        )
        (result,) = (
            fetch_result
            if (fetch_result := cursor.fetchone()) is not None
            else [None]
        )

    if result is None:
        return plaintext("The requested key was not found", 404)

    return plaintext(result)


@app.route("/", methods=["GET"])
def get_root() -> Response:
    return plaintext(
        textwrap.dedent(
            """
            Charlie's simple text storage API

            API:
                POST `/store`
                    Store a string and retreive its key.

                    Parameters (in POST body, as url-encoded):
                        text: The string to store

                    Returns (plaintext):
                        The key that the string can be accessed by

                GET `/<key>`
                    Retreive the string stored under a key.

            Note:
                Offered as-is, I won't get mad if you use it but please don't overdo it, I
                do have to pay for storage and data usage of this server.

                The strings you store with with this API will be saved in plaintext and
                accessible to theoretically anyone (I can't and won't promise this API is
                well-protected against enumeration attacks or the likes).

                Don't use this API for
                -   confidential or otherwise secret stuff
                -   illegal stuff for either you, me or my hosting provider
                -   rude stuff, including bigotry
                -   stuff that's important to you (deletion at any time for any reason)
                -   stuff that's not within reasonable limits of byte size
            """
        ).strip()
    )


if __name__ == "__main__":
    # app.run(host="0.0.0.0", port=80, debug=True)
    serve(app, host="0.0.0.0", port=80)
