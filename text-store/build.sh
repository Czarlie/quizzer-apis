#!/usr/bin/env sh

docker build --no-cache -t text-store "$(dirname "$0")"