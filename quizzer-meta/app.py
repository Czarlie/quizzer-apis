from flask import Flask, Response, request, make_response
from waitress import serve

import requests
import textwrap
import html


TEXT_STORE_URL: str = "https://text-store.cza.li"


app: Flask = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False


def plaintext(text: str, status_code: int = 200) -> Response:
    response: Response = make_response(text, status_code)

    response.mimetype = "text/plain"
    response.charset = "utf-8"

    return response


def meta(title: str, url: str, image_url: str, alt: str) -> Response:
    n: str = "\n"
    bq: str = '\\"'
    return plaintext(
        textwrap.dedent(
            f"""
                <meta property="og:title" content="{html.escape(title.strip(), quote=True).replace(n, ' ')}" />
                <meta property="og:description" content="Play quiz at quizzer.cza.li" />
                <meta property="og:site_name" content="quizzer" />
                <meta property="og:url" content="{html.escape(url.strip(), quote=True).replace(n, ' ')}" />
                <meta property="og:image" content="{html.escape(image_url.strip(), quote=True).replace(n, ' ')}" />
                <meta property="og:image:alt" content="{html.escape(alt.strip(), quote=True).replace(n, ' ')}" />
                <meta property="og:type" content="website" />
            """
        ).strip()
    )


@app.route("/<key>", methods=["GET"])
def generate_meta_tags(key: str) -> Response:
    try:
        quiz = requests.get(f"{TEXT_STORE_URL}/{key}").json()
    except (
        requests.exceptions.JSONDecodeError,
        requests.exceptions.InvalidJSONError,
    ):
        return meta(
            "quizzer", "https://quizzer.cza.li", "https://a.cza.li/ch-256.png"
        )

    title: str = "quizzer"
    url: str = f"https://quizzer.cza.li?{key}"
    image_url: str = "https://a.cza.li/ch-256.png"
    alt: str = "A stylised sans-serif 'ch'. The letter h is segmented to indicate a period."

    if isinstance(quiz, dict):
        if "title" in quiz:
            if isinstance(quiz["title"], str):
                title = quiz["title"]

            elif (
                isinstance(quiz["title"], dict)
                and "" in quiz["title"]
                and isinstance(quiz["title"][""], str)
            ):
                title = f'{quiz["title"][""]}'

        if (
            "questions" in quiz
            and isinstance(quiz["questions"], list)
            and len(quiz["questions"]) > 0
            and "body" in quiz["questions"][0]
            and isinstance(quiz["questions"][0]["body"], list)
        ):
            for body_segment in quiz["questions"][0]["body"]:
                if not isinstance(body_segment, dict):
                    continue

                if (
                    body_segment.get("type") == "image"
                    and "url" in body_segment
                    and isinstance(body_segment["url"], str)
                ):
                    image_url = body_segment["url"]
                    alt = "No alt text provided for this image"

                    if "alt" in body_segment:
                        if isinstance(body_segment["alt"], str):
                            alt = body_segment["alt"]
                        elif (
                            isinstance(body_segment["alt"], dict)
                            and "" in body_segment["alt"]
                            and isinstance(body_segment["alt"][""], str)
                        ):
                            alt = body_segment["alt"][""]

                    break

    return meta(title, url, image_url, alt)


@app.get("/")
def generate_meta_tags_query_params():
    if len(request.args.keys()):
        return generate_meta_tags([*request.args.keys()][0])

    return plaintext("")


if __name__ == "__main__":
    # app.run(host="0.0.0.0", port=80, debug=True)
    serve(app, host="0.0.0.0", port=80)
